﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmailWithWJT.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ItemController : ControllerBase
	{
		[Authorize]
		[HttpGet]
		[Route("Hello")]
		public string hello()
		{
			return "Hello";
		}

		[Authorize (Roles = "Admin")]
		[HttpGet]
		[Route("ValidateToken")]
		public IActionResult ValidateToken()
		{
			return Ok("Congrats, your token is right");
		}
	}
}
