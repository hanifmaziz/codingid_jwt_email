﻿using EmailWithWJT.Models;
using EmailWithWJT.Services.EmailService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;

namespace EmailWithWJT.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RegisterController : ControllerBase
	{
		private readonly IConfiguration? _config;
		private readonly IEmailService? _emailservice;

		public RegisterController(IConfiguration config, IEmailService emailService)
		{
			_config = config;
			_emailservice = emailService;

		}
		

		[HttpPost]
		[Route("User")]
		public IActionResult Register(UserModel users)
		{
			try
			{
				using (SqlConnection con = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
				{
					string query = "INSERT INTO USERS values('" + users.UserName + "','" + users.Password + "','" + users.Email + "','" + users.Role + "','" + users.Surename + "','" + users.GivenName + "') ";
					con.Open();
					SqlCommand cmd = new SqlCommand(query, con);
					cmd.ExecuteNonQuery();
					con.Close();
				}
				EmailDTO request = new EmailDTO();
				request.Subject = "Verify Link";
				request.To = "hanifmaziz98@gmail.com";
				request.Body = "<a href=\"google.com\">Verify This Link</a>";
				_emailservice.SendEmail(request);

				return Ok("Successfully Register and Sending Email");
			}
			catch (Exception ex)
			{
				return Ok(ex.ToString());
			}
		}
	}
}
