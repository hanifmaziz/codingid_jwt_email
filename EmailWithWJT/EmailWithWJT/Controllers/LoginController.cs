﻿using BCrypt.Net;
using EmailWithWJT.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace EmailWithWJT.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class LoginController : ControllerBase
	{
		private readonly IConfiguration _config;
		public LoginController(IConfiguration config)
		{
			_config = config;
		}
		[AllowAnonymous]
		[HttpPost]
		public IActionResult Login([FromBody] UserLogin userLogin)
		{
			var user = Authenticate(userLogin);

			if (user != null)
			{
				var token = Generate(user);
				return Ok(token);
			}

			return NotFound("User Not Found");
		}

		private string Generate(UserModel user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.NameIdentifier, user.UserName),
				new Claim(ClaimTypes.Email, user.Email),
				new Claim(ClaimTypes.GivenName, user.GivenName),
				new Claim(ClaimTypes.Surname, user.Surename),
				new Claim(ClaimTypes.Role, user.Role),
			};

			var token = new JwtSecurityToken(_config["Jwt:Issuer"],
				_config["Jwt:Audience"],
				claims,
				expires: DateTime.Now.AddMinutes(3),
				signingCredentials: credentials
				);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}
		private UserModel Authenticate(UserLogin userLogin)
		{
			var hashPassword = BCrypt.Net.BCrypt.HashPassword(userLogin.Password);

			List<UserModel> users = new List<UserModel>();

			using (SqlConnection conn = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string query = "SELECT * FROM USERS";
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				dataAdapter.Fill(dt);

				foreach(DataRow row in dt.Rows)
				{
					UserModel user = new UserModel();
					user.UserName = row["UserName"].ToString();
					user.Email = row["Email"].ToString();
					user.Surename = row["Surename"].ToString();
					user.GivenName = row["GivenName"].ToString();
					user.Role = row["Role"].ToString();
					users.Add(user);
				}
			}

			//var currentUser = UserConstant.Users.FirstOrDefault(o => o.UserName.ToLower() == userLogin.UserName.ToLower() &&
			//BCrypt.Net.BCrypt.Verify(userLogin.Password, hashPassword));
			var currentUser = users.FirstOrDefault(o => o.UserName.ToLower() == userLogin.UserName.ToLower() &&
			BCrypt.Net.BCrypt.Verify(userLogin.Password, hashPassword));

			if (currentUser != null)
			{
				return currentUser;
			}
			return null;
		}

	}
}
