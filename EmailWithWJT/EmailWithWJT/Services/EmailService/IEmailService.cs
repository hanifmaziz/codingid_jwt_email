﻿using EmailWithWJT.Models;

namespace EmailWithWJT.Services.EmailService
{
	public interface IEmailService
	{
		void SendEmail(EmailDTO request);
	}
}
